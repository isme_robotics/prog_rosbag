# Programmatic ROS Bag recording

Package aimed at calling the service RosBagCmd for recording the rosbags.

The topic that will be recorder are the ones contain in the config/topics.conf file.
In order to use the package one should:

1. Launch the **prog_record** node:
    - `rosrun prog_rosbag prog_record _topic_names_file:=<file-path> _bag_prefix:=<prefix>`
2. Start recording:
	  - `rosservice call /record_bag "cmd: 'start'"`
3. Stop recording:
    - `rosservice call /record_bag "cmd: 'stop'"`
4. Optional - Loading the config/topics.conf file runtime:
    - `rostopic pub -1 /recorder/load_topics_name std_msgs/Bool "data: true"`

In the topic `/Logs/BagFileRecording` is published a boolean stating whether the recording has started .
