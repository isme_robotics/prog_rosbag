#include <boost/filesystem.hpp>
#include <fstream>
#include <prog_rosbag/pt_recorder.h>
#include <prog_rosbag/Futils.h>
#include <ros/ros.h>
#include <std_msgs/UInt8.h>
#include <string>
#include <std_msgs/Bool.h>
#include "prog_rosbag/RosbagCmd.h"

const std::string bag_file_recording = "/Logs/BagFileRecording";
static bool shouldStartBag = false;
static bool shouldStopBag = false;
static bool b_record = false;
static bool loadTopic = false;
static const std::string load_topic_name_ros_topic = "/recorder/load_topics_name";
static std::shared_ptr<ros::Publisher> ros_bag_pub_ptr;
void extractTopicNames(PRecorderOptions* opts, std::string const* topic_name_file);
bool record_service(prog_rosbag::RosbagCmd::Request& req, prog_rosbag::RosbagCmd::Response& res);
void LoadTopicNameCallBack(const std_msgs::BoolConstPtr& msg);

int main(int argc, char** argv)
{
    ros::init(argc, argv, "prog_record");
    ros::NodeHandle nh;
    ros::NodeHandle nh_private("~");

    while (!ros::ok()) {
    }
    ros::ServiceServer service_srv = nh.advertiseService("record_bag", record_service);
    ros::Publisher ros_bag_pub = nh.advertise<std_msgs::Bool>(bag_file_recording.c_str(),10);
    ros::Subscriber load_topic_sub = nh.subscribe(load_topic_name_ros_topic,1,LoadTopicNameCallBack);
    ros_bag_pub_ptr = std::make_shared<ros::Publisher>(ros::Publisher(ros_bag_pub));
    std::string topic_names_file, data_dir, bag_prefix;
    std::string save_path = futils::get_homepath() + "/rosbag_records";

    nh_private.param<std::string>("topic_names_file", topic_names_file, "");
    nh_private.param<std::string>("data_directory", data_dir, save_path);
    nh_private.param<std::string>("bag_prefix", bag_prefix, "");

    // Handle the output directory already existing
    boost::filesystem::path out_dir(data_dir.c_str());
    std::cout << "Save Directory: " << data_dir << std::endl;

    // Check for existance
    if (!boost::filesystem::exists(out_dir)) {
        ROS_WARN("Output directory doesn't exists, creating it.");
        boost::filesystem::create_directory(out_dir);
    }

    // Create rosbag options
    PRecorderOptions opts;

    // Extract topic names, if there are any
    if (topic_names_file == "") {
        ROS_WARN("Received no topic_names_file, recording all.");
        opts.record_all = true;
    } else {
        // Parse out topic names
        extractTopicNames(&opts, &topic_names_file);

        // Sanity check in case topic format is wrong
        if (opts.topics.empty()) {
            opts.record_all = true;
        }
    }

    // Handle file directory/prefix parameters
    if (data_dir == "") {
        ROS_WARN("Received no data base directory, using ~/.ros");
    } else {

        opts.prefix = data_dir + "/";
    }

    if (bag_prefix == "") {
        ROS_WARN("Received no bag_prefix, ignoring.");
    } else {
        opts.prefix += bag_prefix;
    }

    ROS_INFO("Saving bags to %s_<date-time>", opts.prefix.c_str());

    ros::Rate loop_rate(10);

    // Run the recorder
    PRecorder recorder(opts);
    int result;

    // Run while master is still running
    while (ros::ok()) {

        // Handle starting of bag recording
        if (shouldStartBag) {
            result = recorder.run();
            if (!result) {
                ROS_INFO("Starting bag for topics found in %s", topic_names_file.c_str());
            } else {
                ROS_INFO("Error starting bag.");
            }
            shouldStartBag = false;
        }
        if (shouldStopBag) {
            recorder.stop();
            shouldStopBag = false;
        }
        if(loadTopic){
            extractTopicNames(&opts, &topic_names_file);
            ROS_INFO("[RecorderNode] Loading topics name");
            loadTopic = false;

        }

        loop_rate.sleep();
        ros::spinOnce();
    }
    return 1;
}

void extractTopicNames(PRecorderOptions* opts, std::string const* topic_names_file)
{

    std::ifstream fd(topic_names_file->c_str());
    std::string line;

    if (!fd) {
        ROS_ERROR("Topic input file name invalid, recording everything");
        opts->record_all = true;
    } else {
        while (std::getline(fd, line)) {
            opts->topics.push_back(line);
        }
    }
}

bool record_service(prog_rosbag::RosbagCmd::Request& req, prog_rosbag::RosbagCmd::Response& res)
{
    std::string cmd = req.cmd;
    std_msgs::Bool msg;
    if (cmd == "start") {
        if (b_record) {
            res.res = "Requested 'start', but alredy recording";
        } else {
            msg.data = true;
            ros_bag_pub_ptr->publish(msg);
            b_record = true;
            shouldStartBag = true;
            res.res = "Starting recorder";
        }
        ROS_WARN_STREAM(res.res);
        return true;
    } else if (cmd == "stop") {
        if (b_record) {
            msg.data = false;
            ros_bag_pub_ptr->publish(msg);
            b_record = false;
            shouldStopBag = true;
            res.res = "Stopping recorder";
        } else {
            res.res = "Requested 'stop', but no bag running";
        }
        ROS_WARN_STREAM(res.res);
        return true;
    } else {
        res.res = "No such command \'" + cmd + "\' in [/record/record_bag]. Available options: start, stop";
        ROS_WARN_STREAM(res.res);
        return true;
    }
}

void LoadTopicNameCallBack(const std_msgs::BoolConstPtr& msg){
    loadTopic = msg->data;


}
